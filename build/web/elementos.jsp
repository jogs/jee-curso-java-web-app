<html>
    <head>
        <title>JavaServer Pages</title>
    </head>
    <body>
        <p>Contenidos din&aacute;micos creados utilizando JSP</p>
        <ul>
            <li><b>Expresi�n:</b><br>
                Hostname: <%= request.getRemoteHost() %>.
            <li><b>Scriptlet:</b><br>
                <% out.println("Datos GET enviados: " + request.getQueryString()); %>
            <li><b>Declaraci�n:</b><br>
                <%! private int contadorAccesos = 0; %>
                Accesos a la p�gina desde su creaci�n: <%= ++contadorAccesos %>
            <li><b>Directiva:</b><br>
                <%@ page import = "java.util.*" %>
                Fecha actual: <%= new Date() %>
        </ul>
    </body>
</html>