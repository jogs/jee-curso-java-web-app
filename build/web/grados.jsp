<%@ page import="java.text.*" %>
<html>
    <head>
        <title>Conversi&oacute;n de Grados Farenheit a Grados Celsius</title>
    </head>
    <body>
        <table border="1" cellpadding="3">
            <tr>
                <th>Grados<br>Farenheit</th>
                <th>Grados<br>Celsius</th>
            </tr>
            <%
                NumberFormat nf = new DecimalFormat("##0.000" );
                for (int f=32; f<=212; f+=20)
                {
                    double c=((f-32)*5)/9.0;
                    String cs=nf.format(c);
            %>
            <tr>
                <td align="right"> <%=f %> </td>
                <td align="right"> <%=cs %></td>
            </tr>
            <% 
                }
            %>
        </table>
    </body>
</html>