<!doctype html>
<html>
    <head>
        <title>Factorial del 1 al 10</title>
    </head>
    <body>
        <%-- declaraci�n, funci�n factorial --%>
        <%! public long factorial (long numero)
        {
            if (numero == 0) return 1;
            else return numero * factorial(numero - 1);
        } %>
        <h1>Factoriales de los 10 primeros numeros</h1>
        <p><i>x    x!<i></p>
        <%-- scriptlet, inicia for --%>
        <% for (long x = 0; x <=10; ++x) { %>
            <%--Expresiones para imprimir x y el factorial de x--%>
            <%= x %>    <%= factorial (x) %><br>
        <%-- Scriplet para cerrar for --%>
        <% } %>
    </body>
</html>

