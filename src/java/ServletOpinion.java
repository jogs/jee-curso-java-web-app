import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;


public class ServletOpinion extends HttpServlet
{
    private String nombre=null;
    private String apellidos=null;
    private String opinion=null;
    private String comentarios=null;
    
    
    public void init(ServletConfig config) throws ServletException
    {
        super.init(config);
        System.out.println("Iniciando ServletOpinion...");
    }

    public void destroy()
    {
       System.out.println("No hay nada que hacer...");
    }

    public void doPost (HttpServletRequest req, HttpServletResponse resp)
                       throws ServletException, IOException
    {
	String tipoContenido;
        tipoContenido = req.getContentType();
        System.out.println("Tipo de contenido: " + tipoContenido);
        nombre=req.getParameter("nombre");
        apellidos=req.getParameter("apellidos");
        opinion=req.getParameter("opinion");
        comentarios=req.getParameter("comentarios");
        devolverPaginaHTML(resp);
                
    }
    public void devolverPaginaHTML(HttpServletResponse resp)
    {
        resp.setContentType("text/html");
        PrintWriter out = null;
        try
        {
            out=resp.getWriter();
        } catch (IOException io)
        {
            System.out.println("Se ha producido una excepcion");
        }

        out.println("<html>");
        out.println("<head>");
        out.println("<title>Valores recogidos en el formulario</title>");
        out.println("</head>");
        out.println("<body>");
        out.println("<b><font size=+2>Valores recogidos del ");
        out.println("formulario: </font></b>");
        out.println("<p><font size=+1><b>Nombre: </b>"+nombre+"</font>");
        out.println("<br><fontsize=+1><b>Apellido: </b>"
                    + apellidos+"</font><b><font size=+1></font></b>");
        out.println("<p><font size=+1> <b>Opini&oacute;n: </b><i>" + opinion
                    + "</i></font>");
        out.println("<br><font size=+1><b>Comentarios: </b>" + comentarios
                    + "</font>");
        out.println("</body>");
        out.println("</html>");
        out.flush();
        out.close();
    }

    public String getServletInfo()
    {
        return "Este servlet lee los datos de un formulario" 
        + " y los muestra en pantalla";
    }
}
