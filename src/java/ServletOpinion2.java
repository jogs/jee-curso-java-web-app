import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;
import java.sql.*;

public class ServletOpinion2 extends HttpServlet
{
	private String nombre = null;
	private String apellidos = null;
	private String opinion = null;
	private String comentarios = null;
	Connection conn = null;

	public void init(ServletConfig config) throws ServletException
	{
		super.init(config);
		
		String urlBD = "jdbc:mysql://localhost:3306/opiniones";
		
		try
		{
            Class.forName("com.mysql.jdbc.Driver");
        } catch(ClassNotFoundException ex)
          {
          	System.out.println("Error al cargar el driver");
          	System.out.println(ex.getMessage());
     	  }

		try
		{
      	    conn = DriverManager.getConnection(urlBD, "root", "skypunch");
        } catch (SQLException sqlEx)
          {
              System.out.println("Se ha producido un error al" 
                                  + " establecer la conexión con: " + urlBD);
              System.out.println(sqlEx.getMessage());
          }

		System.out.println("Iniciando ServletOpinion (version BD)...");
	}


	public void destroy()
	{
		super.destroy();
		System.out.println("Cerrando conexion...");
		try
		{
		    conn.close();
		} catch(SQLException ex)
		  {
		      System.out.println("No se pudo cerrar la conexion");
		      System.out.println(ex.getMessage());
		  }
	}

    public void doPost (HttpServletRequest req, HttpServletResponse resp) 
                        throws ServletException, IOException
    {

		boolean hayError = false;
		
		if(req.getParameter("nombre")!=null)
		    nombre = req.getParameter("nombre");
		else
		    hayError=true;
		
		if(req.getParameter("apellidos")!=null)
		    apellidos = req.getParameter("apellidos");
		else
		    hayError = true;
		
		if(req.getParameter("opinion")!=null)
		    opinion = req.getParameter("opinion");
		else
		    hayError = true;
		
		if(req.getParameter("comentarios")!=null)
		    comentarios = req.getParameter("comentarios");
		else
		    hayError = true;
		
		if(!hayError)
		{
		    if (actualizarBaseDeDatos() == 0)
		        devolverPaginaHTML(resp);
		    else
		        resp.sendError(500, "Se ha producido un error"
		                       + " al actualizar la base de datos");
		}
		else
		    resp.sendError(500, "Se ha producido un error"
		                   + " en la adquisici�n de par�metros");
	}
	
	public int actualizarBaseDeDatos()
	{
		Statement stmt=null;
		int numeroFilasActualizadas=0;
		try
		{
		    stmt = conn.createStatement();
		    numeroFilasActualizadas = stmt.executeUpdate("INSERT INTO"
		                + " Opiniones VALUES"+"('"+nombre+ "','"+apellidos
		                + "','"+opinion+"','"+comentarios+"')");
		
		    if(numeroFilasActualizadas!=1)
		        return -1;
		} catch (SQLException sql)
		  {
		      System.out.println("Se produjo un error creando Statement");
		      System.out.println(sql.getMessage());
		      return -2;
		  }
		  finally
		  {
		      if(stmt!=null)
		      {
		          try
		          {
		              stmt.close();
		          } catch(SQLException e)
		            {
		                System.out.println("Error cerrando Statement");
		                System.out.println(e.getMessage());
		                return -3;
		            }
		      }
		      return 0;
		  }
	}

    public void devolverPaginaHTML(HttpServletResponse resp)
    {
        PrintWriter out=null;
        try
        {
            out=resp.getWriter();
        } catch (IOException io)
          {
              System.out.println("Se ha producido una excepcion");
          }

        resp.setContentType("text/html");
        out.println("<html>");
        out.println("<head>");
        out.println("<title>Valores introducidos en el formulario</title>");
        out.println("</head>");
        out.println("<body>");
        out.println("<b><font size=+2>Valores le&iacute;dos del");
        out.println("formulario: </font></b>");
        out.println("<p><font size=+1><b>Nombre: </b>" + nombre + "</font>");
        out.println("<br><fontsize=+1><b>Apellido : </b>"
                    + apellidos + "</font><b><font size=+1> </font></b>");
        out.println("<p><font size=+1><b>Opini&oacute;n: "
                    + "</b><i>" + opinion+" </i></font>");
        out.println("<br><font size=+1><b>Comentarios: </b>"
                    + comentarios + "</font>");
        out.println("<P><hr><center><h2>Valores actualizados "
                    + "con �xito </center>");
        out.println("</body>");
        out.println("</html>");

        out.flush();
        out.close();
    }

    public String getServletInfo()
    {
        return "Este servlet lee los datos de un formulario "
               + "y los introduce en una base da datos";
    }
}